# training_final_project

This is the final training project.
Using AWS services like SNS, SQS, DynamoDB and Lambda with Serverless Framework.

If you want to try it out, make sure to execute **npm i** to install all dependencies.
